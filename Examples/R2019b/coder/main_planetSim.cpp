/* Hand-written main file that demonstrates the 
integration of two generated code projects by using
namespaces. */

#include "stdio.h"
#include <cmath>

/* Modify these lines to specify the correct paths to
your packaged (packNGo) function header files.*/
#include "../projectMoon/getGravityConst.h"
#include "../projectEarth/getGravityConst.h"

int main(int, const char * const[])
{
	double t_m, t_e, d, g; 

	d = 10;

	g = moon::getGravityConst();
	t_m = std::sqrt(2*d/-g);

	printf("\nTime to drop 10 meters on moon: %1.2f s\n", t_m);
	printf("Expected answer:                3.51 s\n\n");

	g = earth::getGravityConst();
	t_e = std::sqrt(2*d/-g);

	printf("Time to drop 10 meters on earth: %1.2f s\n", t_e);
	printf("Expected answer:                 1.43 s\n\n");

	return 0;
}

/* NOTE:
To build this program on Windows, the linker must have
knowledge of the import library (.lib file) names
and their directories. The compiler must have
knowledge of the include directories. The executable must have
access to the dynamic library files. */