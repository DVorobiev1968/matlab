%% Pass Data by Reference
%
% This example shows how to pass data by reference to and from an
% external C function.
%
% Pass by reference is an important technique for C/C++ code integration.
% When you pass data by reference, the program does not need to copy data
% from one function to another. 
% With pass by value, C code can return only a single scalar variable. With
% pass by reference, C code can return multiple variables, including
% arrays. 
%
% Consider the MATLAB function |adderRef|. This function uses 
% external C code to add two arrays. The |coder.rref| and 
% |coder.wref| commands instruct the code generator to pass pointers to the
% arrays, rather than copy them. 
%
% <include>adderRef.m</include> 
%
% The C code, |cAdd.c|, uses linear indexing to access the elements of the
% arrays:
% 
% <include>cAdd.c</include>
%
% To build the C code you must provide a header file, |cAdd.h|, with the function
% signature:
%
% <include>cAdd.h</include>
%
% Test the C code by generating a MEX function and comparing its 
% output with the output from the addition operation in MATLAB.

A = rand(2,2)+1;
B = rand(2,2)+10;

codegen adderRef -args {A, B} cAdd.c cAdd.h -report

if (adderRef_mex(A,B) - (A+B) == 0)
    fprintf(['\n' 'adderRef was successful.']);
end


%% 
% Copyright 2012 The MathWorks, Inc.