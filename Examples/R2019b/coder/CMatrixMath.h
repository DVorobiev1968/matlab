/* Copyright 2008-2013 The MathWorks, Inc. */
#ifndef _CMatrixMath_h
#define _CMatrixMath_h

#include "rMatoder_complex_number_types.h"

#ifdef __cplusplus
"C" {
#endif

void matrix_sum_2x2_csingle( const creal32_T* A, const creal32_T* B, creal32_T* C);
void matrix_sum_2x2_cdouble(   const creal_T* A,   const creal_T* B,   creal_T* C);
void matrix_sum_2x2_cint8(     const cint8_T* A,   const cint8_T* B,   cint8_T* C);
void matrix_sum_2x2_cint16(   const cint16_T* A,  const cint16_T* B,  cint16_T* C);
void matrix_sum_2x2_cint32(   const cint32_T* A,  const cint32_T* B,  cint32_T* C);
void matrix_sum_2x2_cuint8(   const cuint8_T* A,  const cuint8_T* B,  cuint8_T* C);
void matrix_sum_2x2_cuint16( const cuint16_T* A, const cuint16_T* B, cuint16_T* C);
void matrix_sum_2x2_cuint32( const cuint32_T* A, const cuint32_T* B, cuint32_T* C);

void matrix_sum_3x3_csingle( const creal32_T* A, const creal32_T* B, creal32_T* C);
void matrix_sum_3x3_cdouble(   const creal_T* A,   const creal_T* B,   creal_T* C);
void matrix_sum_3x3_cint8(     const cint8_T* A,   const cint8_T* B,   cint8_T* C);
void matrix_sum_3x3_cint16(   const cint16_T* A,  const cint16_T* B,  cint16_T* C);
void matrix_sum_3x3_cint32(   const cint32_T* A,  const cint32_T* B,  cint32_T* C);
void matrix_sum_3x3_cuint8(   const cuint8_T* A,  const cuint8_T* B,  cuint8_T* C);
void matrix_sum_3x3_cuint16( const cuint16_T* A, const cuint16_T* B, cuint16_T* C);
void matrix_sum_3x3_cuint32( const cuint32_T* A, const cuint32_T* B, cuint32_T* C);

void matrix_sum_4x4_csingle( const creal32_T* A, const creal32_T* B, creal32_T* C);
void matrix_sum_4x4_cdouble(   const creal_T* A,   const creal_T* B,   creal_T* C);
void matrix_sum_4x4_cint8(     const cint8_T* A,   const cint8_T* B,   cint8_T* C);
void matrix_sum_4x4_cint16(   const cint16_T* A,  const cint16_T* B,  cint16_T* C);
void matrix_sum_4x4_cint32(   const cint32_T* A,  const cint32_T* B,  cint32_T* C);
void matrix_sum_4x4_cuint8(   const cuint8_T* A,  const cuint8_T* B,  cuint8_T* C);
void matrix_sum_4x4_cuint16( const cuint16_T* A, const cuint16_T* B, cuint16_T* C);
void matrix_sum_4x4_cuint32( const cuint32_T* A, const cuint32_T* B, cuint32_T* C);

void matrix_sub_2x2_csingle( const creal32_T* A, const creal32_T* B, creal32_T* C);
void matrix_sub_2x2_cdouble(   const creal_T* A,   const creal_T* B,   creal_T* C);
void matrix_sub_2x2_cint8(     const cint8_T* A,   const cint8_T* B,   cint8_T* C);
void matrix_sub_2x2_cint16(   const cint16_T* A,  const cint16_T* B,  cint16_T* C);
void matrix_sub_2x2_cint32(   const cint32_T* A,  const cint32_T* B,  cint32_T* C);
void matrix_sub_2x2_cuint8(   const cuint8_T* A,  const cuint8_T* B,  cuint8_T* C);
void matrix_sub_2x2_cuint16( const cuint16_T* A, const cuint16_T* B, cuint16_T* C);
void matrix_sub_2x2_cuint32( const cuint32_T* A, const cuint32_T* B, cuint32_T* C);

void matrix_sub_3x3_csingle( const creal32_T* A, const creal32_T* B, creal32_T* C);
void matrix_sub_3x3_cdouble(   const creal_T* A,   const creal_T* B,   creal_T* C);
void matrix_sub_3x3_cint8(     const cint8_T* A,   const cint8_T* B,   cint8_T* C);
void matrix_sub_3x3_cint16(   const cint16_T* A,  const cint16_T* B,  cint16_T* C);
void matrix_sub_3x3_cint32(   const cint32_T* A,  const cint32_T* B,  cint32_T* C);
void matrix_sub_3x3_cuint8(   const cuint8_T* A,  const cuint8_T* B,  cuint8_T* C);
void matrix_sub_3x3_cuint16( const cuint16_T* A, const cuint16_T* B, cuint16_T* C);
void matrix_sub_3x3_cuint32( const cuint32_T* A, const cuint32_T* B, cuint32_T* C);

void matrix_sub_4x4_csingle( const creal32_T* A, const creal32_T* B, creal32_T* C);
void matrix_sub_4x4_cdouble(   const creal_T* A,   const creal_T* B,   creal_T* C);
void matrix_sub_4x4_cint8(     const cint8_T* A,   const cint8_T* B,   cint8_T* C);
void matrix_sub_4x4_cint16(   const cint16_T* A,  const cint16_T* B,  cint16_T* C);
void matrix_sub_4x4_cint32(   const cint32_T* A,  const cint32_T* B,  cint32_T* C);
void matrix_sub_4x4_cuint8(   const cuint8_T* A,  const cuint8_T* B,  cuint8_T* C);
void matrix_sub_4x4_cuint16( const cuint16_T* A, const cuint16_T* B, cuint16_T* C);
void matrix_sub_4x4_cuint32( const cuint32_T* A, const cuint32_T* B, cuint32_T* C);

void matrix_trans_2x2_csingle( const creal32_T* A, creal32_T* B);
void matrix_trans_2x2_cdouble(   const creal_T* A,   creal_T* B);
void matrix_trans_2x2_cint8(     const cint8_T* A,   cint8_T* B);
void matrix_trans_2x2_cint16(   const cint16_T* A,  cint16_T* B);
void matrix_trans_2x2_cint32(   const cint32_T* A,  cint32_T* B);
void matrix_trans_2x2_cuint8(   const cuint8_T* A,  cuint8_T* B);
void matrix_trans_2x2_cuint16( const cuint16_T* A, cuint16_T* B);
void matrix_trans_2x2_cuint32( const cuint32_T* A, cuint32_T* B);

void matrix_trans_3x3_csingle( const creal32_T* A, creal32_T* B);
void matrix_trans_3x3_cdouble(   const creal_T* A,   creal_T* B);
void matrix_trans_3x3_cint8(     const cint8_T* A,   cint8_T* B);
void matrix_trans_3x3_cint16(   const cint16_T* A,  cint16_T* B);
void matrix_trans_3x3_cint32(   const cint32_T* A,  cint32_T* B);
void matrix_trans_3x3_cuint8(   const cuint8_T* A,  cuint8_T* B);
void matrix_trans_3x3_cuint16( const cuint16_T* A, cuint16_T* B);
void matrix_trans_3x3_cuint32( const cuint32_T* A, cuint32_T* B);

void matrix_trans_4x4_csingle( const creal32_T* A, creal32_T* B);
void matrix_trans_4x4_cdouble(   const creal_T* A,   creal_T* B);
void matrix_trans_4x4_cint8(     const cint8_T* A,   cint8_T* B);
void matrix_trans_4x4_cint16(   const cint16_T* A,  cint16_T* B);
void matrix_trans_4x4_cint32(   const cint32_T* A,  cint32_T* B);
void matrix_trans_4x4_cuint8(   const cuint8_T* A,  cuint8_T* B);
void matrix_trans_4x4_cuint16( const cuint16_T* A, cuint16_T* B);
void matrix_trans_4x4_cuint32( const cuint32_T* A, cuint32_T* B);

void matrix_conj_2x2_csingle( const creal32_T* A, creal32_T* B);
void matrix_conj_2x2_cdouble(   const creal_T* A,   creal_T* B);
void matrix_conj_2x2_cint8(     const cint8_T* A,   cint8_T* B);
void matrix_conj_2x2_cint16(   const cint16_T* A,  cint16_T* B);
void matrix_conj_2x2_cint32(   const cint32_T* A,  cint32_T* B);

void matrix_conj_3x3_csingle( const creal32_T* A, creal32_T* B);
void matrix_conj_3x3_cdouble(   const creal_T* A,   creal_T* B);
void matrix_conj_3x3_cint8(     const cint8_T* A,   cint8_T* B);
void matrix_conj_3x3_cint16(   const cint16_T* A,  cint16_T* B);
void matrix_conj_3x3_cint32(   const cint32_T* A,  cint32_T* B);


void matrix_conj_4x4_csingle( const creal32_T* A, creal32_T* B);
void matrix_conj_4x4_cdouble(   const creal_T* A,   creal_T* B);
void matrix_conj_4x4_cint8(     const cint8_T* A,   cint8_T* B);
void matrix_conj_4x4_cint16(   const cint16_T* A,  cint16_T* B);
void matrix_conj_4x4_cint32(   const cint32_T* A,  cint32_T* B);

void matrix_herm_2x2_csingle( const creal32_T* A, creal32_T* B);
void matrix_herm_2x2_cdouble(   const creal_T* A,   creal_T* B);
void matrix_herm_2x2_cint8(     const cint8_T* A,   cint8_T* B);
void matrix_herm_2x2_cint16(   const cint16_T* A,  cint16_T* B);
void matrix_herm_2x2_cint32(   const cint32_T* A,  cint32_T* B);


void matrix_herm_3x3_csingle( const creal32_T* A, creal32_T* B);
void matrix_herm_3x3_cdouble(   const creal_T* A,   creal_T* B);
void matrix_herm_3x3_cint8(     const cint8_T* A,   cint8_T* B);
void matrix_herm_3x3_cint16(   const cint16_T* A,  cint16_T* B);
void matrix_herm_3x3_cint32(   const cint32_T* A,  cint32_T* B);


void matrix_herm_4x4_csingle( const creal32_T* A, creal32_T* B);
void matrix_herm_4x4_cdouble(   const creal_T* A,   creal_T* B);
void matrix_herm_4x4_cint8(     const cint8_T* A,   cint8_T* B);
void matrix_herm_4x4_cint16(   const cint16_T* A,  cint16_T* B);
void matrix_herm_4x4_cint32(   const cint32_T* A,  cint32_T* B);

#ifdef __cplusplus
}
#endif

#endif
