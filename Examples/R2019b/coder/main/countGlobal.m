function go = countGlobal %#codegen
% increment global variable
global g
g = g+1;
% set output variable
go = double(g);