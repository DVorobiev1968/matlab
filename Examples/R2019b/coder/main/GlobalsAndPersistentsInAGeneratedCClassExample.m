%% Globals and Persistents in a Generated C++ Class
% When you generate C++ code with a class interface, then you access 
% globals and persistents as members of the class. This example shows how
% to interact with globals and persistents in the class. 

% Copyright 2019 The MathWorks, Inc.

%% MATLAB Algorithm
% Consider a MATLAB function that keeps count of the number of times you
% call it with a global and persistent variable. 
%
% <include>countCalls.m</include>
%
%% Generate C++ Code with a Class Interface
% For code generation, initialize the global variable in the workspace. 

global g;
g = 0;

%%
% Generate code in the class called |countClass|.  

cfg = coder.config('lib');
cfg.GenCodeOnly = true;
cfg.TargetLang = 'C++';
cfg.CppInterfaceStyle = 'Methods';
cfg.CppInterfaceClassName = "countClass";
codegen countCalls -config cfg -report

%% Inspect the Class Definition
% In the generated C++ code, an initialization function sets the global
% variable to the value that you specify in the workspace. You can also
% specify the initial global value with the |codegen -globals| syntax. 
%
% Inspect the code for the class definition in the header file
% |countClass.h|.
%
%   type codegen/lib/countCalls/countClass.h
%
% The global variable is a public member of the class.
% Access this variable from your main function as needed. The persistent
% variable is stored in a private class data structure. 
