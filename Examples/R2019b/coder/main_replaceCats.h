#ifndef MAIN_H
#define MAIN_H

#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "replaceCats_types.h"

extern int main(int argc, char *argv[]);

#endif
