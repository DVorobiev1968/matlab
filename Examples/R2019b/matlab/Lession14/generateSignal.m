function generateSignal()
i = [1:1000]; w=2*pi.*i./1000;
Data = sin(w)+sin(3.*w)./3+sin(5.*w)./5+sin(7.*w)./7;
plot(i,Data);