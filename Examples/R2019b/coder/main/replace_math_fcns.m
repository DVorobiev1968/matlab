function [y1, y2] = replace_math_fcns(u1, u2) %#codegen

%   Copyright 2008-2010 The MathWorks, Inc.

y1 = single(atan2(u1,u2));
y2 = double(cos(double(u1)) + sin(double(u2)));
