function [y] = mySubFn(x)
%#codegen
coder.cinclude('foo.h');
coder.updateBuildInfo('addSourceFiles', 'foo.c');
% Pre-initialize y to double type.
y = 0;
y = coder.ceval('foo',x);
end

