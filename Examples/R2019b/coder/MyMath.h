/* Copyright 2010-2013 The MathWorks, Inc. */
#ifndef MyMath_h
#define MyMath_h

#include "rtwtypes.h"

int16_T s16_add_s16_s16(int16_T a, int16_T b);
int16_T s16_sub_s16_s16(int16_T a, int16_T b);

double sin_double( double u );
double cos_double( double u );
float atan2_single( float u1, float u2 );

real_T myScalarFunction_impl(real_T u1, real_T u2);
void myMatrixFunction_impl(real_T* y1, const real_T* u1, const real_T* u2);
void myComplexFunction_impl(creal_T* y1, const creal_T* u1, real_T u2);
#endif
