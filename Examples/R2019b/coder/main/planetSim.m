function [t_m, t_e] = planetSim %#codegen
% Example function that integrates generated code projects with namespaces
d = 10; 
g = 0;

% Add #include statements for external function names
coder.cinclude('../projectMoon/getGravityConst.h');
coder.cinclude('../projectEarth/getGravityConst.h');

% Call external functions & do computations
g = coder.ceval('moon::getGravityConst');
t_m = timeToDrop(d,g);

g = coder.ceval('earth::getGravityConst');
t_e = timeToDrop(d,g);

function t = timeToDrop(d,g)
t = sqrt(2*d/(-g));
