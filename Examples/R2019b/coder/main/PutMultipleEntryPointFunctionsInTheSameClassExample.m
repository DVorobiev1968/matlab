%% Put Multiple Entry-Point Functions in the Same Class
% When you generate C++ code for multiple entry-point functions and use
% the class interface setting, then each function becomes a public method
% of the same class. You can use this technique to create a simpler
% interface to your multiple entry-point function project.

% Copyright 2019 The MathWorks, Inc.

%% MATLAB Entry-Point Functions
% Break the function |countCalls| in the previous example into two, so that
% one function counts the calls with a persistent variable and the other
% counts the calls with a global variable. Inspect the two functions.
%
% <include>countPersistent.m</include>
%
% <include>countGlobal.m</include>
%
%% Generate C++ Code
% Use the |codegen| command and specify the initial global variable value 
% as an input. 

cfg = coder.config('lib');
cfg.GenCodeOnly = true;
cfg.TargetLang = 'C++';
cfg.CppInterfaceStyle = 'Methods';
cfg.CppInterfaceClassName = 'countClassMulti';
codegen countGlobal countPersistent -config cfg -report -globals {'g',0}

%% Inspect the Generated Code
% To see the generated class definition, open |countClassMulti.h|. 
% Each entry-point function is a public method of the class. 
%
%   type codegen/lib/countGlobal/countClassMulti.h
%