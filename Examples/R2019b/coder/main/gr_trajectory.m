% object = gr_trajectory(m,object,dt)
% Given a center mass (distorts space-time to represent gravity) move
% the object along the geodesic in this space-time using the delta time step
% 'dt'.
function object = gr_trajectory(m,object,dt) %#codegen

%   Copyright 2010 The MathWorks, Inc.

% Extract properties from object
p1 = object(1); % t
p2 = object(2); % r
p3 = object(3); % theta
p4 = object(4); % phi
v1 = object(5); % dt
v2 = object(6); % dr
v3 = object(7); % dtheta
v4 = object(8); % dphi

% Update velocity and position (using Euler's method)
% Velocity and position are four dimensional. The first element in the
% vector is the clock/time, the other elements represent position/velocity
% in polar coordinates.
p = [p1 p2 p3 p4];
v = [v1 v2 v3 v4];
p = p + v*dt;
acc = gr_acceleration(m,p2,p3,p4,v);
v = v + acc*dt;

% Update object
object(1) = p(1);
object(2) = p(2);
object(3) = p(3);
object(4) = p(4);
object(5) = v(1);
object(6) = v(2);
object(7) = v(3);
object(8) = v(4);
