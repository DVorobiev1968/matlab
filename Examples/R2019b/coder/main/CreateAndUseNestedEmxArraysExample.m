%% Create and Use Nested |emxArray| Data
% This example shows how to work with generated code that 
% contains |emxArray| data nested inside of other |emxArray| data.
% To use the generated code, in your main function or calling function,
% initialize the |emxArray| data from the bottom nodes up.
%
% Copyright 2018 The MathWorks, Inc.

%% MATLAB Algorithm
% This MATLAB algorithm iterates through an array of structures
% called |myarray|. Each structure contains a lower-level array of values.
% The algorithm sorts and sum the elements of the lower-level array for
% each |struct|.
%
% <include>processNestedArrays.m</include>
%
%% Generate MEX Function for Testing
% As a first step, to be able to test the algorithm, generate a MEX 
% function. Use the |coder.typeof| function to manually specify the 
% input as an unbounded, variable-size row vector of |structs|, 
% which themselves contain unbounded, variable-size row vectors. 

myarray = coder.typeof( ...
            struct('values', coder.typeof(0, [1 inf]), ...
                   'sorted', coder.typeof(0, [1 inf]), ...
                   'sum', coder.typeof(0))                , [1 inf]);
codegen -args {myarray} processNestedArrays

%% Inspect the Generated Function Interfaces
% The MEX function
% source code contains specialized code that enables it to interface
% with the MATLAB runtime environment, which makes it more complex to read. 
% To produce more simplified source code, generate library code.

codegen -config:lib -args {myarray} processNestedArrays -report

%%
% Inspect the generated function code |processNestedArrays.c| from the 
% code generation report. The
% generated example main file |main.c| shows how to call the generated 
% function code by creating and initializing inputs with the |emxCreate|
% API function.

%% Write and Use Your Own Customized Main File to Initialize |emxArray| Data
% Although the generated example main shows how to invoke the generated
% function code, it does not contain information on desired input 
% values. Using the example main as a guide,
% write your own main file. Use the coding style and preferences
% of your choice. Specify the values of your inputs and
% insert pre and post-processing code as needed.
%
% The file |processNestedArrays_main.c| shows an example.
% This main file uses the |emxArray| API functions to create and initialize
% the structure data. For both the generated example main file and this
% hand written main file, the code initializes the
% |emxArray| data at the bottom (leaf) nodes, and assigns that data to the
% nodes above. 

type processNestedArrays_main.c 

%% Generate an Executable and Compare Results with MEX Function
% Using the provided main file, you can generate a standalone executable
% for the algorithm.

codegen -config:exe -args {myarray} processNestedArrays ...
    processNestedArrays_main.c -report

%%
% Declare input data for the MEX function that matches the input for the
% standalone executable, defined in |processNestedArrays_main.c|.

myarray = [struct('values', [5 3 4 1 2 6], 'sorted', zeros(1,0), 'sum', 0), ...
           struct('values', [50 30 40 10 20 60], 'sorted', zeros(1,0), 'sum', 0), ...
           struct('values', [42 4711 1234], 'sorted', zeros(1,0), 'sum', 0)];

%%
% Compare the MEX function results with the
% standalone executable results.

fprintf('.mex output \n----------- \n');
r = processNestedArrays_mex(myarray);
disp(r(1));
disp(r(2));
disp(r(3));

fprintf('.exe output \n----------- \n');
system('processNestedArrays');

%%
% The output results are identical. 








