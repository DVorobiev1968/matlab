function hLib = crl_table_row_major_op

hLib = RTW.TflTable;
libPath = fullfile(fileparts(mfilename('fullpath')));
hdrFile = 'CRL_Multiplication.h';
srcFile = 'CRL_Multiplication.c';

%---------- entry: RTW_OP_MUL_CUSTOM ----------- 
hEnt = TflCustomMatrixOperationEntry;
hEnt.setTflCOperationEntryParameters( ...
          'Key', 'RTW_OP_MUL', ...
          'Priority', 100, ...
          'SaturationMode', 'RTW_WRAP_ON_OVERFLOW', ...
          'ImplementationName', 'MyMul_CUSTOM_ROW', ...
          'ImplementationHeaderFile', hdrFile, ...
          'ImplementationSourceFile', srcFile, ...
          'ImplementationHeaderPath', libPath, ...
          'ImplementationSourcePath', libPath, ...
          'GenCallback',              'RTW.copyFileToBuildDir' ...
          );

% Conceptual Args

arg = RTW.TflArgMatrix('y1', 'RTW_IO_OUTPUT',  'double');
arg.DimRange = [1 1; Inf Inf];
hEnt.addConceptualArg(arg);

arg = RTW.TflArgMatrix('u1', 'RTW_IO_INPUT',  'double');
arg.DimRange = [1 1; Inf Inf];
hEnt.addConceptualArg(arg);

arg = RTW.TflArgMatrix('u2', 'RTW_IO_INPUT',  'double');
arg.DimRange = [1 1; Inf Inf];
hEnt.addConceptualArg(arg);

% Implementation Args 

arg = hEnt.getTflArgFromString('unused','void');
arg.IOType = 'RTW_IO_OUTPUT';
hEnt.Implementation.setReturn(arg); 

arg = hEnt.getTflArgFromString('u1','double*');
hEnt.Implementation.addArgument(arg);

arg = hEnt.getTflArgFromString('u2','double*');
hEnt.Implementation.addArgument(arg);

arg = hEnt.getTflArgFromString('y1','double*');
arg.IOType = 'RTW_IO_OUTPUT';
hEnt.Implementation.addArgument(arg);

arg = hEnt.getTflArgFromString('size_u1_1st','uint32',0);
hEnt.Implementation.addArgument(arg);

arg = hEnt.getTflArgFromString('size_u1_2nd','uint32',0);
hEnt.Implementation.addArgument(arg);

arg = hEnt.getTflArgFromString('size_u2_1st','uint32',0);
hEnt.Implementation.addArgument(arg);

arg = hEnt.getTflArgFromString('size_u2_2nd','uint32',0);
hEnt.Implementation.addArgument(arg);


hEnt.ArrayLayout='ROW_MAJOR';
hLib.addEntry( hEnt );


%---------- entry: RTW_OP_ELEM_MUL_CUSTOM ----------- 
hEnt = TflCustomMatrixOperationEntry;
hEnt.setTflCOperationEntryParameters( ...
    'Key', 'RTW_OP_ELEM_MUL', ...
    'Priority', 100, ...
    'SaturationMode', 'RTW_WRAP_ON_OVERFLOW', ...
    'ImplementationName', 'MyElemMul_CUSTOM_ROW', ...
    'ImplementationHeaderFile', hdrFile, ...
    'ImplementationSourceFile', srcFile, ...
    'ImplementationHeaderPath', libPath, ...
    'ImplementationSourcePath', libPath, ...
    'GenCallback',              'RTW.copyFileToBuildDir' ...
    );


% Conceptual Args

arg = RTW.TflArgMatrix('y1', 'RTW_IO_OUTPUT',  'double');
arg.DimRange = [1 1; Inf Inf];
hEnt.addConceptualArg(arg);

arg = RTW.TflArgMatrix('u1', 'RTW_IO_INPUT',  'double');
arg.DimRange = [1 1; Inf Inf];
hEnt.addConceptualArg(arg);

arg = RTW.TflArgMatrix('u2', 'RTW_IO_INPUT',  'double');
arg.DimRange = [1 1; Inf Inf];
hEnt.addConceptualArg(arg);

% Implementation Args 

arg = hEnt.getTflArgFromString('unused','void');
arg.IOType = 'RTW_IO_OUTPUT';
hEnt.Implementation.setReturn(arg); 

arg = hEnt.getTflArgFromString('u1','double*');
hEnt.Implementation.addArgument(arg);

arg = hEnt.getTflArgFromString('u2','double*');
hEnt.Implementation.addArgument(arg);

arg = hEnt.getTflArgFromString('y1','double*');
arg.IOType = 'RTW_IO_OUTPUT';
hEnt.Implementation.addArgument(arg);

arg = hEnt.getTflArgFromString('size_u1_1st','uint32',0);
hEnt.Implementation.addArgument(arg);

arg = hEnt.getTflArgFromString('size_u1_2nd','uint32',0);
hEnt.Implementation.addArgument(arg);

arg = hEnt.getTflArgFromString('size_u2_1st','uint32',0);
hEnt.Implementation.addArgument(arg);

arg = hEnt.getTflArgFromString('size_u2_2nd','uint32',0);
hEnt.Implementation.addArgument(arg);


hEnt.ArrayLayout='ROW_MAJOR';
hLib.addEntry( hEnt );

%---------- entry: RTW_OP_ADD ----------- 
hEnt = TflCustomMatrixOperationEntry;
hEnt.setTflCOperationEntryParameters( ...
    'Key', 'RTW_OP_ADD', ...
    'Priority', 100, ...
    'SaturationMode', 'RTW_WRAP_ON_OVERFLOW', ...
    'ImplementationName', 'MyAdd_CUSTOM_ROW', ...
    'ImplementationHeaderFile', hdrFile, ...
    'ImplementationSourceFile', srcFile, ...
    'ImplementationHeaderPath', libPath, ...
    'ImplementationSourcePath', libPath, ...
    'GenCallback',              'RTW.copyFileToBuildDir' ...
    );

% Conceptual Args

arg = RTW.TflArgMatrix('y1', 'RTW_IO_OUTPUT',  'double');
arg.DimRange = [1 1; Inf Inf];
hEnt.addConceptualArg(arg);

arg = RTW.TflArgMatrix('u1', 'RTW_IO_INPUT',  'double');
arg.DimRange = [1 1; Inf Inf];
hEnt.addConceptualArg(arg);

arg = RTW.TflArgMatrix('u2', 'RTW_IO_INPUT',  'double');
arg.DimRange = [1 1; Inf Inf];
hEnt.addConceptualArg(arg);

% Implementation Args 

arg = hEnt.getTflArgFromString('unused','void');
arg.IOType = 'RTW_IO_OUTPUT';
hEnt.Implementation.setReturn(arg); 

arg = hEnt.getTflArgFromString('u1','double*');
hEnt.Implementation.addArgument(arg);

arg = hEnt.getTflArgFromString('u2','double*');
hEnt.Implementation.addArgument(arg);

arg = hEnt.getTflArgFromString('y1','double*');
arg.IOType = 'RTW_IO_OUTPUT';
hEnt.Implementation.addArgument(arg);

arg = hEnt.getTflArgFromString('size_u1_1st','uint32',0);
hEnt.Implementation.addArgument(arg);

arg = hEnt.getTflArgFromString('size_u1_2nd','uint32',0);
hEnt.Implementation.addArgument(arg);

arg = hEnt.getTflArgFromString('size_u2_1st','uint32',0);
hEnt.Implementation.addArgument(arg);

arg = hEnt.getTflArgFromString('size_u2_2nd','uint32',0);
hEnt.Implementation.addArgument(arg);

hEnt.ArrayLayout='ROW_MAJOR';
hLib.addEntry( hEnt );
