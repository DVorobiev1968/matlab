function y = getmyfield()
% Copyright 2018 The MathWorks, Inc.
%#codegen

global g;
y = g.a;
end