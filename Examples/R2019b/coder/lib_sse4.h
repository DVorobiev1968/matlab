/* Copyright 2016 The MathWorks, Inc. */
#ifndef lib_sse4_h_
#define lib_sse4_h_

#include <stddef.h>

float myDotProd_f32_sse4(const float* u1, const float* u2, const size_t n);

#endif /*lib_sse4_h_*/
