
void MyAdd_CUSTOM_ROW(const double *u1, const double *u2, double *y1, unsigned int u1_first,
        unsigned int u1_second, unsigned int u2_first, unsigned int u2_second);

void MyAdd_CUSTOM_COL(const double *u1, const double *u2, double *y1, unsigned int u1_first,
        unsigned int u1_second, unsigned int u2_first, unsigned int u2_second);

void MyElemMul_CUSTOM_ROW(const double *u1, const double *u2, double *y1, unsigned int u1_first,
        unsigned int u1_second, unsigned int u2_first, unsigned int u2_second);

void MyElemMul_CUSTOM_COL(const double *u1, const double *u2, double *y1, unsigned int u1_first,
        unsigned int u1_second, unsigned int u2_first, unsigned int u2_second);

void MyMul_CUSTOM_ROW(const double *u1, const double *u2, double *y1, unsigned int u1_first,
        unsigned int u1_second, unsigned int u2_first, unsigned int u2_second);

void MyMul_CUSTOM_COL(const double *u1, const double *u2, double *y1, unsigned int u1_first,
        unsigned int u1_second, unsigned int u2_first, unsigned int u2_second);