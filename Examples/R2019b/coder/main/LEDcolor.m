classdef LEDcolor < int32
    enumeration
        GREEN(1),
        RED(2)
    end
end