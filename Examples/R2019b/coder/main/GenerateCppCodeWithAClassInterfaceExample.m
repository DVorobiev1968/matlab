%% Generate C++ Code with a Class Interface
% This example shows how the generated C++ code differs when it uses a
% class interface.

% Copyright 2019 The MathWorks, Inc.

%% MATLAB Algorithm
% Consider a simple MATLAB function that performs operations on a matrix
% and outputs the result.
%
% <include>foog.m</include>
%
%% Generate C++ Code With and Without Class Interface
% To generate C++ code with a class interface, use the 
% |CppInterfaceStyle| and |CppInterfaceClassName| parameters. 
% Store the output in the |withClass| folder. 

cfg = coder.config('lib');
cfg.GenCodeOnly = true;
cfg.TargetLang = 'C++';
cfg.CppInterfaceStyle = 'Methods';
cfg.CppInterfaceClassName = 'myClass';
codegen foog -config cfg -report -d withClass

%% 
% Next, create a new configuration object and 
% generate C++ code that does not use a class interface.

cfg = coder.config('lib');
cfg.GenCodeOnly = true;
cfg.TargetLang = "C++";
codegen foog -config cfg -report -d withoutClass

%%
% Inspect the generated example main function. Compare the versions with
% and without the class interface. With the class interface, the main 
% function calls the entry-point function as a class method.
%
%   type withClass/examples/main.cpp
%
%% Class Definition and Implementation in the Generated Code
% When the code generator produces code for the C++ interface class, it
% ensures that the function methods are reentrant. If the function methods
% use variables that can exceed the local stack memory limit, set by the
% configuration parameter |StackUsageMax|, then the code generator produces
% private data structures for the variables (identifiable by the suffix |StackData|),
% rather than declaring the variables as |static|. Static variables persist 
% between function calls and are not reentrant. For information on 
% generating reentrant C code, see <docid:coder_doccenter#btcl13s-17>.
%
% To explore the generated class implementations, modify the function |foog|
% such that it contains a variable that exceeds the maximum stack usage 
% specified by the configuration parameter |StackUsageMax|. 
%%
%
% <include>foogBig.m</include>
%
%% 
% The default value for |StackUsageMax| in bytes is:

cfg.StackUsageMax

%%
% Because |fooBig| uses a variable of 448^2 (200704) elements, and the code
% generator produces an 8-bit integer array to represent the variable, the default
% stack usage limit is exceeded by 704 bytes. Generate code for |foogBig|. 

cfg = coder.config('lib','ecoder',false);
cfg.GenCodeOnly = true;
cfg.TargetLang = 'C++';
cfg.CppInterfaceStyle = 'Methods';
cfg.CppInterfaceClassName = 'myBigClass';
codegen foogBig -config cfg -report -d withBigClass

%% Inspect the Generated Interface Class Definitions
% Inspect the class definitions for the |foogBig| project and for |foog|. 
% The |foogBig| class stores variables that can exceed the maximum stack
% usage in a private class property, whereas the |foog| class only creates
% local variables on the stack. 
%
% When you work with a class definition that contains a |StackData| structure,
% indicating that the class requires data that exceeds the local stack
% usage limit, then allocate heap memory for the class instance by using |new|. 
% See the generated example main file for your generated code for an
% example.



