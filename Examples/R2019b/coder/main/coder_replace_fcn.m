function [y1, y2, y3] = coder_replace_fcn(u1, u2) %#codegen
%   Copyright 2012 The MathWorks, Inc.

y1 = myScalarFunction(u1(1),u1(2));
y2 = myMatrixFunction(u1, u2);
complexValue = u1 + 1i;
y3 = myComplexFunction(complexValue, u2(1));
end


%-----------------------------------------
% Lookup this function for CRL replacement
% Function: myScalarFunction
% Inputs:  u1: double scalar
%          u2: double scalar
% Outputs: y1: double scalar
%----------------------------------------
function y1 = myScalarFunction(u1, u2)

% Trigger CRL lookup 
coder.replace('-errorifnoreplacement');
y1 = sin(u1) + cos(u2);
end

%-----------------------------------------
% Lookup this function for CRL replacement
% Function: myMatrixFunction
% Inputs:  u1: 2x2 double matrix
%          u2: 2x2 double matrix
% Outputs: y1: 2x2 double matrix
%----------------------------------------
function y1 = myMatrixFunction(u1, u2)

% Trigger CRL lookup 
coder.replace('-errorifnoreplacement');
y1 = u1 + u2;
end


%-----------------------------------------
% Lookup this function for CRL replacement
% Function: myComplexFunction
% Inputs:  u1: 2x2 complex double matrix
%          u2: double scalar
% Outputs: y1: 2x2 complex double matrix
%-----------------------------------------
function y1 = myComplexFunction(u1, u2)

% Trigger CRL lookup 
coder.replace('-errorifnoreplacement');
y1 = u1 + u2;
end
