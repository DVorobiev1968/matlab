%% Name the Structure Type That Represents a Heterogeneous Cell Array
% This example shows how to name the structure type that represents a heterogeneous cell array in the generated code.
%
%% Cell Array Is an Entry-Point Function Input
% Create a |coder.CellType| object for a heterogeneous cell array. For
% example:
T = coder.typeof({'c', 1})
%%
% Use |coder.cstructname| to create a copy of |T| that specifies the name |myname| for the structure
% type that represents the cell array in the generated code.
T = coder.cstructname(T, 'myname')
%%
% You can use |coder.cstructname| to create a |coder.CellType| object for a 
% heterogeneous cell array from a |coder.CellType| object for a fixed-size,
% homogeneous cell array.
%%
% Create a |coder.CellType| object for a fixed-size homogeneous cell array.
T = coder.typeof({0, 1})
%%
% Create a copy of |T| that represents a  heterogenous cell array by  
% using |coder.cstructname|.
T = coder.cstructname(T, 'myname')
%%
% T is a |coder.CellType| object for a heterogeneous cell array.
%% Cell Array Is Not an Entry-Point Function Input
%%
% Define a function that defines a heterogeneous cell array. Use |coder.cstructname|
% to specify the name of the structure type. For example:
%
% <include>mycell.m</include>
%%
% Generate code for |mycell|. For example, generate a static library.
codegen -config:lib mycell
%%
% View the generated structure type |myname|.
type codegen/lib/mycell/mycell_types.h
%%
% If a cell array is a fixed-size homogeneous cell array, you can use
% |coder.cstructname| to force it to be a heterogeneous. For example:
%
% <include>mycell1.m</include>


%% 
% Copyright 2012 The MathWorks, Inc.