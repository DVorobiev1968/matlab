classdef sysMode < int32
    enumeration
        OFF(0),
        ON(1)
    end
end