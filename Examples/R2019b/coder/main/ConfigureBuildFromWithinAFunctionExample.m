%% Configure Build from Within a Function
% This example shows how to configure the build for external C/C++ code 
% from within a MATLAB(R) function. 
% Configure the build within a function so that you can more easily integrate 
% it with other projects. 
% 
% Suppose that you have a top-level MATLAB function, |myFn|:
%
% <include>myFn.m</include>
%
% This function calls another function, |mySubFn|, that uses 
% the external C code |foo.c|.  
% By using |coder.updateBuildInfo| and |coder.cinclude|, you set 
% all the necessary external code dependencies from within |mySubFn|. 
%
% <include>mySubFn.m</include>
%
% You can generate code containing |mySubFn| without
% needing to configure additional build settings or specify
% external file inputs at the command line.
% To generate code for the top-level function |myFn|, enter: 

codegen myFn -args {5} -report


%% 
% Copyright 2012 The MathWorks, Inc.