%% Generate Code for an LED Control Function That Uses Enumerated Types
% This example shows how to generate code for a function that uses 
% enumerated types. In this example, the enumerated types inherit from
% base type |int32|. The base type can be |int8|, |uint8|, |int16|, 
% |uint16|, or |int32|.
%
% Define the enumerated type |sysMode|.  Store it in |sysMode.m| on the
% MATLAB path.
%
% <include>sysMode.m</include>
%%
% Define the enumerated type |LEDcolor|. Store it in |LEDcolor.m| on the
% MATLAB path.
%
% <include>LEDcolor.m</include>
%%
% Define the function |displayState|, which uses enumerated data to activate 
% an LED display, based on the state of a device. 
% |displayState| lights a green LED display to indicate the ON state.
% It lights a red LED display to indicate the OFF state. 
%
% <include>displayState.m</include>
%%
% Generate a MEX function for |displayState|. Specify that
% |displayState| takes one input argument that has an enumerated data type
% |sysMode|.
%%
codegen displayState -args {sysMode.ON}
%%
% Test the MEX function.
displayState_mex(sysMode.OFF)
%%
% Generate a static library for the function |displayState|. Specify that 
% |displayState| takes one input argument that has an enumerated data type
% |sysMode|.
codegen -config:lib displayState -args {sysMode.ON}
%%
% codegen generates a C static library with the default name,
% |displayState|. It generates supporting files in the default folder, 
% |codegen/lib/displayState|.
%%
% View the header file |displayState_types.h|.
%
type codegen/lib/displayState/displayState_types.h
%%
% The enumerated type |LEDcolor| is represented as a C enumerated type 
% because the base type in the class definition for |LEDcolor| is |int32|.
% When the base type is |int8|, |uint8|, |int16|, or |uint16|, the code
% generator produces a |typedef| for the enumerated type.
% It produces |#define| statements for the enumerated type values. 
% For example:
%
%  typedef short LEDcolor;
%  #define GREEN ((LEDcolor)1)
%  #define RED ((LEDcolor)2)



%% 
% Copyright 2012 The MathWorks, Inc.