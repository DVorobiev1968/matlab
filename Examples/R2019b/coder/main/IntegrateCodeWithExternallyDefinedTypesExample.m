%% Integrate External Code that Uses Custom Data Types
%
% This example shows how to call a C function that uses data types that are 
% not natively defined within MATLAB(R). 
%
% For example, if your C code performs file input or output on a C 'FILE *' type,
% there is no corresponding type within MATLAB. 
% To interact with this data type in your MATLAB code,
% you must initialize it by using the function |coder.opaque|. 
% In the case of structure types, you can use |coder.cstructname|. 
%
% For example, consider the MATLAB function |addCTypes.m|. This function 
% uses |coder.ceval| with input types defined in external code.
% The function |coder.opaque| initializes the type in MATLAB.
%
% <include>addCTypes.m</include>
%
% The |createStruct| function outputs a C structure type:
%
% <include>createStruct.c</include>
%
% The |useStruct| function performs an operation on the C type: 
%
% <include>useStruct.c</include>
%
% To generate code, specify the source (.c) files as inputs:
%
codegen addCTypes -args {1,2} -report createStruct.c useStruct.c 


%% 
% Copyright 2012 The MathWorks, Inc.