function rtwTargetInfo(tr)

% Copyright 2007-2013 The MathWorks, Inc.

tr.registerTargetInfo(@locTflRegFcn);

end % End of rtwTargetInfo


% Local function(s)
function thisTfl = locTflRegFcn

thisTfl(1) = RTW.TflRegistry;
thisTfl(1).Name = 'Addition & Subtraction Examples'; 
thisTfl(1).Description = 'Demonstration of addition and subtraction operator replacement for built-in integers';
thisTfl(1).TableList = {'crl_table_addsub'};
thisTfl(1).TargetHWDeviceType = {'*'};

thisTfl(2) = RTW.TflRegistry;
thisTfl(2).Name = 'Function Replacement Examples'; 
thisTfl(2).Description = 'Demonstration of function replacement';
thisTfl(2).TableList = {'crl_table_functions'};
thisTfl(2).TargetHWDeviceType = {'*'};

thisTfl(3) = RTW.TflRegistry;
thisTfl(3).Name = 'Matrix Op Replacement Examples'; 
thisTfl(3).Description = 'Demonstration of matrix operation replacement';
thisTfl(3).TableList = {'crl_table_matops'};
thisTfl(3).TargetHWDeviceType = {'*'};

thisTfl(4) = RTW.TflRegistry;
thisTfl(4).Name = 'BLAS Replacement Examples'; 
thisTfl(4).Description = 'Demonstration of matrix operation replacement';
thisTfl(4).TableList = {'crl_table_blas'};
thisTfl(4).TargetHWDeviceType = {'*'};

thisTfl(5) = RTW.TflRegistry;
thisTfl(5).Name = 'Coder Replace Examples'; 
thisTfl(5).Description = 'Demonstration of coder replace';
thisTfl(5).TableList = {'crl_table_coder_replace'};
thisTfl(5).TargetHWDeviceType = {'*'};

thisTfl(6) = RTW.TflRegistry;
thisTfl(6).Name = 'Row Major CRL Examples'; 
thisTfl(6).Description = 'Demonstration of row-major vs column-major operation replacement';
thisTfl(6).TableList = {'crl_table_row_major_op','crl_table_column_major_op'};
thisTfl(6).TargetHWDeviceType = {'*'};
end % End of LOCTFLREGFCN

% EOF
