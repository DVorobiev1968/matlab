%% Specify Configuration Parameters for MEX Function Generation
%%
% Write a MATLAB&#0174; function from which you can generate code. This
% example uses the function |myadd| that returns the sum of its inputs.
%
% <include>myadd.m</include>
%%
% Create a configuration object for MEX function generation.
cfg = coder.config('mex');
%%
% Change the values of the properties for which you do not want to use the
% default value. For example, enable just-in-time (JIT) compilation.
cfg.EnableJIT = true;
%%
% Generate code using |codegen|. Pass the configuration object to |codegen|
% by using the |-config| option. Specify that the input arguments are scalar
% double.
codegen myadd -config cfg -args {1 1}

%% 
% Copyright 2012 The MathWorks, Inc.