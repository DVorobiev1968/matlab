/* Copyright 2016 The MathWorks, Inc. */
#ifndef lib_sse2_h_
#define lib_sse2_h_

#include <stddef.h>

void myMinus_f32_sse2(const float* u1, const float* u2, float* y1, const size_t n);

#endif /*lib_sse2_h_*/
