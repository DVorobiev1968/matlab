function c = myFunction(a,b)
c = sqrt(square(a)+square(b));
end

function y = square(x)
y = x.^2;
end