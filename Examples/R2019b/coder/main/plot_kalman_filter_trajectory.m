%   Copyright 2010-2016 The MathWorks, Inc.
function plot_kalman_filter_trajectory(z,y)

% First, setup the figure
numPts = size(z,2);
figure;hold;grid;            % Prepare plot window
title('Trajectory of object [blue] its Kalman estimate [green]');
xlabel('horizontal position');
ylabel('vertical position');
axis([-1.1, 1.1, -1.1, 1.1]);

% Main loop
for idx = 1: numPts
    plot(z(1,idx), z(2,idx), 'bx-');
    plot(y(1,idx), y(2,idx), 'go-');
    pause(0.02);
    
    %     formatSpec = 'Y = %f %f   Z = %f %f\n';
    %     fprintf(1,formatSpec,y(1,idx),y(2,idx),z(1,idx),z(2,idx));
end
end