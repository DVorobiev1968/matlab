function [y1, differences] = biased_sum_of_square_differences(u1,bias)

%   Copyright 2016 The MathWorks, Inc.

[y1,differences] = sum_of_square_differences(u1+bias);

end
