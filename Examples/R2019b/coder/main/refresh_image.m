% refresh_image(im, cdata)
% Update the image object with the color matrix in 'cdata'.
function refresh_image(im, cdata) %#codegen

%   Copyright 2010 The MathWorks, Inc.

coder.extrinsic('set', 'drawnow');
set(im, 'CData', cdata);
drawnow;
