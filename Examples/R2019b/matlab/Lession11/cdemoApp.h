/*
 * MATLAB Compiler: 7.1 (R2019b)
 * Date: Mon Aug 16 17:03:53 2021
 * Arguments: "-B""macro_default""-l""cdemoApp.m"
 */

#ifndef cdemoApp_h
#define cdemoApp_h 1

#if defined(__cplusplus) && !defined(mclmcrrt_h) && defined(__linux__)
#  pragma implementation "mclmcrrt.h"
#endif
#include "mclmcrrt.h"
#ifdef __cplusplus
extern "C" { // sbcheck:ok:extern_c
#endif

/* This symbol is defined in shared libraries. Define it here
 * (to nothing) in case this isn't a shared library. 
 */
#ifndef LIB_cdemoApp_C_API 
#define LIB_cdemoApp_C_API /* No special import/export declaration */
#endif

/* GENERAL LIBRARY FUNCTIONS -- START */

extern LIB_cdemoApp_C_API 
bool MW_CALL_CONV cdemoAppInitializeWithHandlers(
       mclOutputHandlerFcn error_handler, 
       mclOutputHandlerFcn print_handler);

extern LIB_cdemoApp_C_API 
bool MW_CALL_CONV cdemoAppInitialize(void);

extern LIB_cdemoApp_C_API 
void MW_CALL_CONV cdemoAppTerminate(void);

extern LIB_cdemoApp_C_API 
void MW_CALL_CONV cdemoAppPrintStackTrace(void);

/* GENERAL LIBRARY FUNCTIONS -- END */

/* C INTERFACE -- MLX WRAPPERS FOR USER-DEFINED MATLAB FUNCTIONS -- START */

extern LIB_cdemoApp_C_API 
bool MW_CALL_CONV mlxCdemoApp(int nlhs, mxArray *plhs[], int nrhs, mxArray *prhs[]);

/* C INTERFACE -- MLX WRAPPERS FOR USER-DEFINED MATLAB FUNCTIONS -- END */

/* C INTERFACE -- MLF WRAPPERS FOR USER-DEFINED MATLAB FUNCTIONS -- START */

extern LIB_cdemoApp_C_API bool MW_CALL_CONV mlfCdemoApp();

#ifdef __cplusplus
}
#endif
/* C INTERFACE -- MLF WRAPPERS FOR USER-DEFINED MATLAB FUNCTIONS -- END */

#endif
