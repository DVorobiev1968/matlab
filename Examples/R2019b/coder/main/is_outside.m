% [o,ox,oy] = is_outside(cdata, ball)
% 'o' is true if 'ball' is partially/fully outside of the 'cdata' color matrix
% 'ox' is true if 'ball' is partially/fully outside of the 'cdata' color matrix
% along the X axis.
% 'oy' is true if 'ball' is partially/fully outside of the 'cdata' color
% matrix along the Y axis.
function [o,ox,oy] = is_outside(cdata, ball) %#codegen

%   Copyright 2010 The MathWorks, Inc.

o = false;
ox = false;
oy = false;
height = size(cdata,1);
width = size(cdata,2);
if round(ball.x) - round(ball.r) < 1 || round(ball.x) + round(ball.r) > width
    o = true;
    ox = true;
end
if round(ball.y) - round(ball.r) < 1 || round(ball.y) + round(ball.r) > height
    o = true;
    oy = true;
end
