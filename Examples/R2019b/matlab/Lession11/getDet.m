function d = getDet(A)
%DET det(A) is the determinant of A.
if isempty(A)
    d = 1;
    return
else
    disp('Else block');
end