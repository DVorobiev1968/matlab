function [y1, y2, y3] = replace_matrix_ops(u1, u2) %#codegen
% This block supports the Embedded MATLAB subset.
% See the help menu for details. 

%   Copyright 2010 The MathWorks, Inc.

y1 = u1 + u2;
y2 = u1 - u2;
y3 = y2';