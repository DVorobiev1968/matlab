
%   Copyright 2016 The MathWorks, Inc.

S1 = RandStream('mt19937ar','Seed',1426);
len = 400000;
global g1;

for idx=1:100
    g1 = single(20*rand(S1,[len,1])); %#ok
    args = {single(20*rand(S1,[len,1])),single(10*rand(S1))};

    result = biased_sum_of_square_differences(args{:}); %#ok<NASGU>
end

clear global g1;
clear idx S1 len args result;
