%% Use |emxArray_char_T| Data with String Inputs
% In this example, a MATLAB function changes the size of a character vector
% at run time.
% Because the final length of the vector can vary, the generated C code
% instantiates the vector as a dynamically sized |emxArray|.
% This example shows how to write a main function that 
% uses |emxArray_char_T| with the generated function interface.
% Use this example as a guide for working with the |emxArray_char_T| data
% type.

% Copyright 2018 The MathWorks, Inc.

%% MATLAB Algorithm
% The function |replaceCats| takes a character vector as input
% and replaces all instances of the word 'cat' or 'Cat' with 'velociraptor'
% and 'Velociraptor'.
% Because the code generator cannot determine the 
% output length at compile time, the generated code uses the |emxArray| data
% type.
%
% <include>replaceCats.m</include>
%
%% Generate Source Code
% To generate code for |replaceCats|, specify the input type to the 
% function as a variable-size character array. 

t = coder.typeof('a',[1 inf]);
codegen replaceCats -args {t} -report -config:lib

%% 
% In the generated code, the example main file 
% |/codegen/lib/replaceCats/examples/main.c| provides a template for
% writing your own main function.
%
%% Create a Main Function from the Template
% Modify the main function to take character input from the command line.
% Use the |emxCreate| and |emxCreateWrapper| 
% API functions to initialize your emxArray data.
% After you have finished writing your main source file and header file,
% place the modified files in the root folder.

type main_replaceCats.c

%% Generate Executable File
% Generate executable code:

t = coder.typeof('a',[1 inf]);
codegen replaceCats -args {t} -config:exe main_replaceCats.c

%%
% Test the executable on your platform and modify your main file 
% as needed. For example, on Windows, you get the output:
%%
% |C:\>replaceCats.exe "The pet owner called themselves a 'Catdad'"|
%%
% |Old C string: The pet owner called themselves a 'Catdad'|
%%
% |New C string: The pet owner called themselves a 'Velociraptordad'|
