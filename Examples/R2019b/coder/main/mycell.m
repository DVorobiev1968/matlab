function y =  mycell()
%#codegen
c = {1 [2 3]};
coder.cstructname(c, 'myname');
y = c;
end