#pragma once
#include <stdint.h>
#include "rtwtypes.h"

typedef struct {
    int32_T * vals;
    int32_T numel;
} myArrayType;

void arrayInit(myArrayType * s, int32_T * temparray);
int32_T arraySum(myArrayType * s);
void arrayDest(myArrayType * s);