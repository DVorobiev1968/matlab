function out = foogBig %#codegen
I = eye(448);
out = ones(448)*I + 7;