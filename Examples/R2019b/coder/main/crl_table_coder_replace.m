function hLib = crl_table_coder_replace

%   Copyright 2012-2013 The MathWorks, Inc.


hLib = RTW.TflTable;

LibPath = fullfile(matlabroot,...
                   'toolbox',...
                   'rtw',...
                   'rtwdemos',...
                   'crl_demo');  

%---------- entry: myScalarFunction ----------- 
hEnt = RTW.TflCFunctionEntry;
hEnt.setTflCFunctionEntryParameters( ...
    'Key', 'myScalarFunction', ...
    'Priority', 100, ...
    'ImplementationName', 'myScalarFunction_impl', ...
    'ImplementationHeaderFile', 'MyMath.h', ...
    'ImplementationSourceFile', 'MyMath.c', ...
    'ImplementationHeaderPath', LibPath, ...
    'ImplementationSourcePath', LibPath, ...
    'SideEffects', true);

% Conceptual Args

arg = hEnt.getTflArgFromString('y1','double');
arg.IOType = 'RTW_IO_OUTPUT';
hEnt.addConceptualArg(arg);

arg = hEnt.getTflArgFromString('u1','double');
hEnt.addConceptualArg(arg);

arg = hEnt.getTflArgFromString('u2','double');
hEnt.addConceptualArg(arg);

% Implementation Args 

arg = hEnt.getTflArgFromString('y1','double');
arg.IOType = 'RTW_IO_OUTPUT';
hEnt.Implementation.setReturn(arg); 

arg = hEnt.getTflArgFromString('u1','double');
hEnt.Implementation.addArgument(arg);

arg = hEnt.getTflArgFromString('u2','double');
hEnt.Implementation.addArgument(arg);

hLib.addEntry( hEnt ); 

%---------- entry: myComplexFunction ----------- 
hEnt = RTW.TflCFunctionEntry;
hEnt.setTflCFunctionEntryParameters( ...
    'Key', 'myComplexFunction', ...
    'Priority', 100, ...
    'ImplementationName', 'myComplexFunction_impl', ...
    'ImplementationHeaderFile', 'MyMath.h', ...
    'ImplementationSourceFile', 'MyMath.c', ...
    'ImplementationHeaderPath', LibPath, ...
    'ImplementationSourcePath', LibPath, ...
    'SideEffects', true);

% Conceptual Args

arg = RTW.TflArgMatrix('y1', 'RTW_IO_OUTPUT',  'cdouble');
arg.DimRange = [2 2];
hEnt.addConceptualArg(arg);

arg = RTW.TflArgMatrix('u1', 'RTW_IO_INPUT',  'cdouble');
arg.DimRange = [2 2];
hEnt.addConceptualArg(arg);

arg = hEnt.getTflArgFromString('u2','double');
hEnt.addConceptualArg(arg);

% Implementation Args 

arg = hEnt.getTflArgFromString('void','void');
arg.IOType = 'RTW_IO_OUTPUT';
hEnt.Implementation.setReturn(arg); 

arg = hEnt.getTflArgFromString('y1','cdouble*');
arg.IOType = 'RTW_IO_OUTPUT';
hEnt.Implementation.addArgument(arg);

arg = hEnt.getTflArgFromString('u1','cdouble*');
hEnt.Implementation.addArgument(arg);

arg = hEnt.getTflArgFromString('u2','double');
hEnt.Implementation.addArgument(arg);

hLib.addEntry( hEnt ); 

%---------- entry: myMatrixFunction ----------- 
hEnt = RTW.TflCFunctionEntry;
hEnt.setTflCFunctionEntryParameters( ...
    'Key', 'myMatrixFunction', ...
    'Priority', 100, ...
    'ImplementationName', 'myMatrixFunction_impl', ...
    'ImplementationHeaderFile', 'MyMath.h', ...
    'ImplementationSourceFile', 'MyMath.c', ...
    'ImplementationHeaderPath', LibPath, ...
    'ImplementationSourcePath', LibPath, ...
    'SideEffects', true);

% Conceptual Args

arg = RTW.TflArgMatrix('y1', 'RTW_IO_OUTPUT',  'double');
arg.DimRange = [2 2];
hEnt.addConceptualArg(arg);

arg = RTW.TflArgMatrix('u1', 'RTW_IO_INPUT',  'double');
arg.DimRange = [2 2];
hEnt.addConceptualArg(arg);

arg = RTW.TflArgMatrix('u2', 'RTW_IO_INPUT',  'double');
arg.DimRange = [2 2];
hEnt.addConceptualArg(arg);

% Implementation Args 

arg = hEnt.getTflArgFromString('void','void');
arg.IOType = 'RTW_IO_OUTPUT';
hEnt.Implementation.setReturn(arg); 

arg = hEnt.getTflArgFromString('y1','double*');
arg.IOType = 'RTW_IO_OUTPUT';
hEnt.Implementation.addArgument(arg);

arg = hEnt.getTflArgFromString('u1','double*');
hEnt.Implementation.addArgument(arg);

arg = hEnt.getTflArgFromString('u2','double*');
hEnt.Implementation.addArgument(arg);

hLib.addEntry( hEnt ); 

