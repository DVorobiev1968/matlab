% y = compute_average(x)
% This function takes an input scalar value 'x' and returns the average
% value so far.
function y = compute_average(x) %#codegen
assert(isa(x,'double')); % Input is scalar double

% Declare two persistent variables 'sum' and 'cnt'.
persistent sum cnt;

% Upon the first call we need to initialize the variables.
if isempty(sum)
    sum = 0;
    cnt = 0;
end

% Compute the accumulated sum and the number of values so far.
sum = sum + x;
cnt = cnt + 1;

% Return the current average.
y = sum / cnt;
