classdef LightModeType < Simulink.IntEnumType
    enumeration
        None(0)
        Red(1)
        Yellow(2)
        Green(3)
    end
end