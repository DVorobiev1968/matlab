% outImage = gr_raytrace_image(inImage, progress)
% Raytrace and return an output image given an input image 'inImage',
% a 'progress' function callback
function outImage = gr_raytrace_image(inImage, imageHandle) %#codegen
coder.extrinsic('set', 'drawnow', 'disp', 'fprintf');
assert(isa(inImage, 'uint8'));
assert(size(inImage, 1) <= 2048);
assert(size(inImage, 2) <= 2048);
assert(size(inImage, 3) == 3);
%   Copyright 2010-2013 The MathWorks, Inc.

% Indicate the progress callback that the rendering process has begun
if nargin < 3
    useParFor = false;
end

% Size of black hole in geometric mass units.
geometric_mass = 5000000000; % geometric mass = G*mass/c/c

outImage = zeros(size(inImage),class(inImage));

% Extract height and width of output image
height = size(outImage,1);
width = size(outImage,2);

imageCls = class(outImage);
imageSize = size(outImage);

update(true, imageSize, imageCls, imageHandle);

% Enable this line to use the parfor version instead. Unfortunately,
% you'll not get incremental progress on rendering the output image.
% useParFor = true;

if useParFor
    parfor y = 1:height
        line = computeLine(inImage, geometric_mass, width, height, imageCls, y);
        outImage(y,:,:) = line;
    end
else
    for y = 1:height
        line = computeLine(inImage, geometric_mass, width, height, imageCls, y);
        outImage(y,:,:) = line;
        update(false, imageSize, imageCls, imageHandle, y, line);
    end
end
set(imageHandle, 'CData', outImage);

function line = computeLine(inImage, geometric_mass, width, height, imageCls, y)
% For each pixel we trace a ray backwards from where it came from.
dt = 2; % Use 2 seconds as the time step.
r = 700; % Radius which denotes the distance of the camera
line = zeros(1, width, 3, imageCls);
for x = 1:width
    %
    % dx and dy span from -0.5 to 0.5 providing a 90 degree view.
    %
    x0 = x;
    y0 = y;
    dx = (x0-1-width/2)/width;
    dy = (y0-1-height/2)/height;
    % Trace ray backwards
    [col,err] = trace_ray(inImage,geometric_mass,r,dx,dy,dt);
    if err
       % Some error occurred, which could be because this
       % coordinate system cannot handle this specific ray.
       % Let's retry with dy = 0
       [col,err] = trace_ray(inImage,geometric_mass,r,dx,0,dt);
       % This still didn't work, so let's use a black pixel.
       if err
           col = uint8([0 0 0]');
       end
    end
    line(1,x,:) = col;
end

% Sub-function to trace a specific ray.
% If the color of the background image cannot be reached, return
% an error.
function [col,err] = trace_ray(inImage,geometric_mass,r,dx,dy,dt)

err = false;

% If radius is 0 then we're in the center of the black hole
if r == 0
    col = uint8([0,0,0]');
    return
end

% Compute the polar coordinates of the given ray provided in Cartesian
% coordinates.
dtheta = -dx/r;
dphi = -dy/r;
dr = sqrt(1-dtheta*dtheta-dphi*dphi);

ray_of_light = [0 r pi/2 pi 0 -dr dtheta dphi];
limit = 300; % Behind black hole
totalR = r + limit;

c = 299792458; % Speed of light
rs = 2*geometric_mass/c;
imSc = min(size(inImage,1),size(inImage,2));

maxIter = 10000; % Rays can loop around the black hole, but we give up after 10000 iterations
for i = 1:maxIter
    r = ray_of_light(2);
    theta = ray_of_light(3);
    phi = ray_of_light(4);

    % Are we inside the Schwarzschild radius? 
    if abs(r) < rs
        col = zeros(3,1,'uint8'); % It's certainly black in a black hole!
        return
    end    
    
    % In Cartesian coordinates (x,y,z)
    x = r*cos(theta);
    y = r*sin(theta)*sin(phi);
    z = r*sin(theta)*cos(phi);

    if z > limit % we hit the background!
        x1 = x/totalR + 0.5; % Normalize x to 0..1
        y1 = y/totalR + 0.5; % Normalize y to 0..1
        imX = int32(x1*imSc) + 1;
        imY = int32(y1*imSc) + 1;
        col = zeros(3,1,'uint8');
        if imX >= 1 && imX <= size(inImage,2) && imY >= 1 && imY <= size(inImage,1)
            col(:) = inImage(imY,imX,:);
        else
            col = uint8([255 255 255]'); % Error
            err = true;
        end
        return
    end

    % Compute next position of ray
    ray_of_light1 = gr_trajectory(geometric_mass/c,ray_of_light,dt);
    err = sum(abs(ray_of_light1 - ray_of_light)) > 2.1;
    if err
        col = uint8([255 255 255]');
        return
    end
    ray_of_light = ray_of_light1;
end
% Something went wrong. We couldn't reach a specific point in the
% background image. Return an error.
col = uint8([255 255 255]');
err = true;

function update(init, imageSize, imageCls, imageHandle, y, line)
coder.extrinsic('set', 'drawnow', 'fprintf');
coder.inline('never');
persistent image lastProgress;
if isempty(image) || init
    image = zeros(imageSize, imageCls);
    lastProgress = 0;
end

if nargin >= 5 && ~isempty(line)
    image(y,:,:) = line;
end

cnt = 0;
for i = 1:size(image,1)
    if image(i,1,1) ~= 0
        cnt = cnt + 1;
    end
end
progress = round(100 * cnt / size(image,1));
if mod(progress,5) == 0
    if progress ~= lastProgress
        lastProgress = progress;
        fprintf('Progress: %d%%\n', progress);
    end
end
set(imageHandle, 'CData', image);
drawnow('update');
