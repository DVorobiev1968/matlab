function [y1, y2 , y3, y4] = replace_mixed_row_column_ops( u1,u2, u3)
coder.rowMajor;
[y1,y2] = Child_Op(u1,u2,u3);
y3 = u1*u2;
y4 = u1+u3;
end

function [b1, b2] = Child_Op( a1,a2,a3)
coder.columnMajor;
coder.inline('never');
b1 = a1*a2;
b2 = a1+a3;
end