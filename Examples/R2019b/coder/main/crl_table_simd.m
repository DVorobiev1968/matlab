function hLib = crl_table_simd

%   Copyright 2016 The MathWorks, Inc.

hLib = RTW.TflTable;
libpath = fullfile(fileparts(which(mfilename)));
src_sse2 = 'lib_sse2.c';
hdr_sse2 = 'lib_sse2.h';
src_sse4 = 'lib_sse4.c';
hdr_sse4 = 'lib_sse4.h';
cc = rtwprivate('getMexCompilerInfo');

%---------- entry: RTW_OP_MINUS ----------- 
hEnt = customEntry.VectorMinus;
hEnt.setTflCOperationEntryParameters( ...
          'Key', 'RTW_OP_MINUS', ...
          'Priority', 100, ...
          'ImplementationName', 'myMinus_f32_sse2', ...
          'ImplementationSourceFile',src_sse2, ...
          'ImplementationSourcePath',libpath, ...
          'ImplementationHeaderFile',hdr_sse2, ...
          'ImplementationHeaderPath',libpath, ...
          'SideEffects', true, ...
          'GenCallback','RTW.copyFileToBuildDir');
if(ismember(cc.comp.Manufacturer,{'GNU','Apple'}))
    hEnt.AdditionalCompileFlags = {'-msse2'};
end

hEnt.EntryInfo.Algorithm = 'RTW_CAST_BEFORE_OP';
% Conceptual Args

arg = RTW.TflArgMatrix('y1', 'RTW_IO_OUTPUT',  'single');
arg.DimRange = [2 1; Inf 1];
hEnt.addConceptualArg(arg);

arg = RTW.TflArgMatrix('u1', 'RTW_IO_INPUT',  'single');
arg.DimRange = [2 1; Inf 1];
hEnt.addConceptualArg(arg);

arg = RTW.TflArgMatrix('u2', 'RTW_IO_INPUT',  'single');
arg.DimRange = [2 1; Inf 1];
hEnt.addConceptualArg(arg);

% Implementation Args 

arg = hEnt.getTflArgFromString('unused','void');
arg.IOType = 'RTW_IO_OUTPUT';
hEnt.Implementation.setReturn(arg); 

arg = hEnt.getTflArgFromString('u1','single*');
des = RTW.ArgumentDescriptor;
des.AlignmentBoundary = 16;
arg.Descriptor = des;
hEnt.Implementation.addArgument(arg);

arg = hEnt.getTflArgFromString('u2','single*');
des = RTW.ArgumentDescriptor;
des.AlignmentBoundary = 16;
arg.Descriptor = des;
hEnt.Implementation.addArgument(arg);

arg = hEnt.getTflArgFromString('y1','single*');
arg.IOType = 'RTW_IO_OUTPUT';
des = RTW.ArgumentDescriptor;
des.AlignmentBoundary = 16;
arg.Descriptor = des;
hEnt.Implementation.addArgument(arg);

arg = hEnt.getTflArgFromString('numElements','uint32',0);
hEnt.Implementation.addArgument(arg);


hLib.addEntry( hEnt ); 


%---------- entry: RTW_OP_MUL - dot product ----------- 
hEnt = customEntry.DotProduct;
hEnt.setTflCOperationEntryParameters( ...
          'Key', 'RTW_OP_MUL', ...
          'Priority', 100, ...
          'ImplementationName', 'myDotProd_f32_sse4', ...
          'ImplementationSourceFile',src_sse4, ...
          'ImplementationSourcePath',libpath, ...
          'ImplementationHeaderFile',hdr_sse4, ...
          'ImplementationHeaderPath',libpath, ...
          'SideEffects', true, ...
          'GenCallback','RTW.copyFileToBuildDir');
if(ismember(cc.comp.Manufacturer,{'GNU','Apple'}))
    hEnt.AdditionalCompileFlags = {'-msse4'};
end

% Conceptual Args

arg = hEnt.getTflArgFromString('y1','single');
arg.IOType = 'RTW_IO_OUTPUT';
hEnt.addConceptualArg(arg);

arg = RTW.TflArgMatrix('u1', 'RTW_IO_INPUT',  'single');
arg.DimRange = [1 2; 1 Inf];
hEnt.addConceptualArg(arg);

arg = RTW.TflArgMatrix('u2', 'RTW_IO_INPUT',  'single');
arg.DimRange = [2 1; Inf 1];
hEnt.addConceptualArg(arg);

% Implementation Args 

arg = hEnt.getTflArgFromString('y1','single');
arg.IOType = 'RTW_IO_OUTPUT';
hEnt.Implementation.setReturn(arg); 

arg = hEnt.getTflArgFromString('u1','single*');
des = RTW.ArgumentDescriptor;
des.AlignmentBoundary = 16;
arg.Descriptor = des;
hEnt.Implementation.addArgument(arg);

arg = hEnt.getTflArgFromString('u2','single*');
des = RTW.ArgumentDescriptor;
des.AlignmentBoundary = 16;
arg.Descriptor = des;
hEnt.Implementation.addArgument(arg);


arg = hEnt.getTflArgFromString('numElements','uint32',0);
hEnt.Implementation.addArgument(arg);

hLib.addEntry( hEnt ); 

