#ifndef MAIN_H
#define MAIN_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "springMassTakeStep_types.h"

/* Function Declarations */
extern int main(int argc, char * argv[]);

#endif

/* End of code generation (main.h) */
