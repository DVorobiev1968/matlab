%% Pass Structure Arguments by Reference or by Value in Generated Code
% This example shows how to control whether structure arguments to
% generated entry-point functions are passed by reference or by value.
%
% Passing by reference uses a pointer to access the structure arguments.
% If the function writes to an element of the input structure,
% it overwrites the input value. Passing by value makes a copy of the input
% or output structure argument. To reduce memory usage and execution time,
% use pass by reference.
%
% If a structure argument is both an input and output, the generated 
% entry-point function passes the argument by reference.
% Generated MEX functions pass structure arguments by reference. For MEX
% function output, you cannot specify that you want to pass structure 
% arguments by value.
%% Specify Pass by Reference or by Value Using the MATLAB Coder App
% To open the *Generate* dialog box, on the *Generate Code* page, click the
% *Generate* arrow.
% 
%% 
% Set the *Build type* to one of the following:
%
% * Source Code
% * Static Library
% * Dynamic Library
% * Executable
%% 
% Click *More Settings*.
%%
% On the *All Settings* tab, set the *Pass structures by reference to
% entry-point functions* option to:
% 
% * Yes, for pass by reference (default)
% * No, for pass by value
%% Specify Pass by Reference or by Value Using the Command-Line Interface
% Create a code configuration object for a static library, a dynamic
% library, or an executable program. For example, create a code 
% configuration object for a static library.
%
%  cfg = coder.config('lib'); 
%%
% Set the |PassStructByReference| property to:
%
% *  |true|, for pass by reference (default)
% *  |false|, for pass by value
%
% For example:
%
%  cfg.PassStructByReference = true;
%% Pass Input Structure Argument by Reference
%% 
% Write the MATLAB function |my_struct_in| that has an input structure 
% argument.
% 
% <include>my_struct_in.m</include>
%%
% Define a structure variable |mystruct| in the MATLAB(R) workspace.
mystruct = struct('f', 1:4);
%%
% Create a code generation configuration object for a C static library.
cfg = coder.config('lib');
%% 
% Specify that you want to pass structure arguments by reference.
cfg.PassStructByReference = true;
%%
% Generate code.  Specify that the input argument has the type of the
% variable |mystruct|.
codegen -config cfg -args {mystruct} my_struct_in 
%% 
% View the generated C code.
type codegen/lib/my_struct_in/my_struct_in.c
%% 
% The generated function signature for |my_struct_in| is
%
%  void my_struct_in(const struct0_T *s, double y[4])
%
% |my_struct_in| passes the input structure |s| by reference.
%% Pass Input Structure Argument by Value
%% 
% Specify that you want to pass structure arguments by value.
cfg.PassStructByReference = false;
%% 
% Generate code.  Specify that the input argument has the type of the
% variable |mystruct|.
codegen -config cfg -args {mystruct} my_struct_in 
%% 
%% 
% View the generated C code.
type codegen/lib/my_struct_in/my_struct_in.c
%% 
%% 
% The generated function signature for |my_struct_in| is
%
%  void my_struct_in(const struct0_T s, double y[4]
%
% |my_struct_in| passes the input structure |s| by value.
%% Pass Output Structure Argument by Reference
%% 
% Write the MATLAB function |my_struct_out| that has an output structure
% argument.
%
% <include>my_struct_out.m</include>
%% 
% Define a variable |a| in the MATLAB(R) workspace.
a = 1:4;
%%
% Create a code generation configuration object for a C static library.
cfg = coder.config('lib');
%% 
% Specify that you want to pass structure arguments by reference.
cfg.PassStructByReference = true;
%%
% Generate code.   Specify that the input argument has the type of the 
% variable |a|.
codegen -config cfg -args {a} my_struct_out
%% 
% View the generated C code.
type codegen/lib/my_struct_out/my_struct_out.c
%%
% The generated function signature for |my_struct_out| is
%
%  void my_struct_out(const double x[4], struct0_T *s)
%
% |my_struct_out| passes the output structure |s| by reference.
%% Pass Output Structure Argument by Value
%% 
% Specify that you want to pass structure arguments by value.
cfg.PassStructByReference = false;
%%
% Generate code.   Specify that the input argument has the type of the 
% variable |a|.
codegen -config cfg -args {a} my_struct_out
%% 
% View the generated C code.
type codegen/lib/my_struct_out/my_struct_out.c
%%
% The generated function signature for |my_struct_out| is
%
%  struct0_T my_struct_out(const double x[4])
%
% |my_struct_out| returns an output structure.
%% Pass Input and Output Structure Argument by Reference
% When an argument is both an input and an output, the generated C function
% passes the argument by reference even when |PassStructByReference|  is
% false.
%% 
% Write the MATLAB function |my_struct_inout| that has a structure
% argument that is both an input argument and an output argument.
%
% <include>my_struct_inout.m</include>
%% 
% Define the variable |a| and structure variable |mystruct| in the MATLAB(R)
% workspace.
a = 1:4;
mystruct = struct('f',a);
%%
% Create a code generation configuration object for a C static library.
cfg = coder.config('lib');
%% 
% Specify that you want to pass structure arguments by value.
cfg.PassStructByReference = false;
%%
% Generate code.   Specify that the first input has the type of |a| and the
% second input has the type of |mystruct|.
codegen -config cfg -args {a, mystruct} my_struct_inout 
%% 
% View the generated C code.
type codegen/lib/my_struct_inout/my_struct_inout.c
%%
% The generated function signature for |my_struct_inout| is
%
%  void my_struct_inout(const double x[4], const struct0_T *s, double y[4])
%
% |my_struct_inout| passes the structure |s| by reference even though 
% |PassStructByReference| is |false|.


%% 
% Copyright 2012 The MathWorks, Inc.