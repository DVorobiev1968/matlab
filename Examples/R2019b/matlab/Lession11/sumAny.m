function result=sumAny(x1,x2,x3)
%������� ������������ � ������������ ������� ����������, � ������ > 3, ��
% ����� ���������� ����� 3 ����������
switch nargin
    case 1 
        result=x1^2;
    case 2
        result=x1^2+x2^2;
    case 3
        result=x1^2+x2^2+x3^2;
    otherwise
        %��������� ������� ������� �� ����� ������, �.�. ����������
        %���������� ��� ��� ������ �������
        warning('Too many parameters')
        result=0;
end