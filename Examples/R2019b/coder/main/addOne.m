function y = addOne(x)
% Copyright 2014 - 2016 The MathWorks, Inc.

%#codegen
y = x + 1;
end