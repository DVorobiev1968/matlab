function [y1, differences] = biased_sum_of_square_differences_da16(u1,bias)

%   Copyright 2016 The MathWorks, Inc.

% Specify alignment for differences since it is normally allocated
% externally
coder.dataAlignment('differences',16);

[y1,differences] = sum_of_square_differences(u1+bias);

end
