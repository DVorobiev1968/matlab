%% Control Whether a MEX Function Checks the Value of a Constant Input
% This example shows how to use the |ConstantInputs| parameter to control
% whether a MEX function checks the value of a constant input argument.
%%
% Write a function |myadd| that returns the sum of its inputs.
%
% <include>myadd.m</include>
%%
% Create a configuration object for MEX code generation.
mexcfg = coder.config('mex');
%%
% Look at the value of the constant input checking configuration parameter,
% |ConstantInputs|.
mexcfg.ConstantInputs
%%
% It has the default value, |CheckValues|.
%%
% Generate a MEX function |myadd_mex|. Specify that the first argument is a
% double scalar and that the second argument is a constant with value |3|.
codegen myadd -config mexcfg -args {1, coder.Constant(3)}
%%
% Call |myadd_mex|. You must provide the input |3| for the second argument.
myadd_mex(1,3)
%%
% Modify |ConstantInputs| so that the MEX function does not check that the
% input value matches the value specified at code generation time.
mexcfg.ConstantInputs = 'IgnoreValues';
%%
% Generate |myadd_mex|.
codegen myadd -config mexcfg -args {1, coder.Constant(3)}
%%
% Call |myadd_mex| with a constant input value other than |3|, for example,
% |5|.
myadd_mex(1,5)
%%
% The MEX function ignores the input value |5|. It uses the value |3|, 
% which is the value that you specified for the constant argument |b|
% when you generated |myadd_mex|.
%%
% Modify |ConstantInputs| so that the MEX function signature does not
% include the constant input argument.
mexcfg.ConstantInputs = 'Remove';
%%
% Generate |myadd_mex|.
codegen myadd -config mexcfg -args {1, coder.Constant(3)}
%%
% Call |myadd_mex|. Provide the value |1| for |a|. Do not provide a value
% for the constant argument |b|.
myadd_mex(1)

%% 
% Copyright 2012 The MathWorks, Inc.