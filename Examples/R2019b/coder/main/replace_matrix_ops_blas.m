function [y1, y2] = replace_matrix_ops_blas(u1, u2) %#codegen
% This block supports the MATLAB Coder subset.
% See the help menu for details. 

%   Copyright 2010 The MathWorks, Inc.

y1 = u1 * u2;
y2 = u1' * u2;
